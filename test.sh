#! /bin/bash

if [ ! -d ~/.rbenv ]; then
	echo "Not exist rbenv."
	exit 1
fi

if [ ! -d ~/.nodebrew ]; then
	echo "Not exist nodebrew."
	exit 1
fi

if [ ! -d ~/.opam ]; then
	echo "Not exist opam."
	exit 1
fi

if [ ! -d ~/Workspace/go ]; then
	echo "Not exist go-path."
	exit 1
fi

exit 0
